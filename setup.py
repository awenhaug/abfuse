"""
Created 06 Sep 2020
Anders Wenhaug, anders.wenhaug@solutionseeker.no
"""

from setuptools import setup


email = "awenhaug@gmail.com"
fullname = "Anders Wenhaug"
name = "abfuse"
version = "0.0.7"

setup(
    name=name,
    version=version,

    description="Jetbrains log demultiplexer",
    long_description="File system that exposes a single file for Jetbrains products to log to, "
                     "but redirects the logs to one file per process.",
    author=fullname,
    author_email=email,
    maintainer=fullname,
    maintainer_email=email,
    license="MIT",
    packages=[
        name,
    ],
    py_modules=[
        "main",
    ],
    entry_points={
        "console_scripts": [
            f"mount.{name} = main:main"
        ]
    },
    url="https://gitlab.com/awenhaug/abfuse",

    classifiers=[
        "Intended Audience :: Developers",
        "Operating System :: MacOS",
        "Operating System :: POSIX",
        "Operating System :: Unix",
        "Programming Language :: Python :: 3",
        "Topic :: System :: Filesystems",
        "License :: OSI Approved :: MIT License",
    ]
)
