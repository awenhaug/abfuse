"""
Created 05 Sep 2020
Anders Wenhaug, anders.wenhaug@solutionseeker.no

See libfuse (e.g. https://github.com/libfuse/libfuse/blob/c5e8684b5a2f3400af6d7a3edcaeb3ce8ffc51b5/include/fuse.h)
for documentation

FIXME: Not much error checking is done
"""

import os
import json
import time
import stat
import errno
import logging
import datetime
import typing as ty

import fuse
import psutil


logging.basicConfig()
logger = logging.getLogger("abfuse")


class Abfuse(fuse.Operations):
    def __init__(self, root: str, fake_file_name: str):
        self._root: str = root
        self._fake_file_name: str = fake_file_name

        self._i: int = 0
        self._file_handle_map: ty.Dict[int, ty.Tuple[psutil.Process, int]] = {}

    def _process_filter(self, processes: ty.List[psutil.Process]) -> ty.List[psutil.Process]:
        """

        :param processes:
        :return:
        """
        return list(filter(lambda process: "python" in process.name().lower(), processes))

    def _get_target_file_handle(self, source_fh) -> int:
        return self._file_handle_map[source_fh][1]

    @property
    def pid(self) -> int:
        return fuse.fuse_get_context()[2]

    def _target_path(self, process: psutil.Process) -> str:
        process_path = self._get_target_name(process)
        while process_path.startswith("/"):
            process_path = process_path[1:]

        return f"{os.path.join(self._root, process_path)}-{datetime.datetime.utcnow().isoformat()}"

    def _source_path(self, partial) -> str:
        return os.path.join("/", partial)

    def getattr(self, path, fh=None) -> ty.Dict:
        if path == '/':
            st = dict(st_mode=(stat.S_IFDIR | 0o755), st_nlink=2)
        elif path == f'/{self._fake_file_name}':
            st = dict(st_mode=(stat.S_IFREG | 0o777), st_size=0)
        else:
            raise fuse.FuseOSError(errno.ENOENT)

        st['st_uid'] = os.getuid()
        st['st_gid'] = os.getgid()

        st['st_ctime'] = st['st_mtime'] = st['st_atime'] = 0

        return st

    def write(self, path, buf, offset, fh) -> int:
        fh = self._get_target_file_handle(fh)
        # NOTE: Is this seek even needed? We've opened the file in append mode anyway
        os.lseek(fh, offset, os.SEEK_SET)
        return os.write(fh, buf)

    def readdir(self, path, fh) -> ty.List[str]:
        return ['.', '..', self._fake_file_name]

    def _get_source_process(self) -> psutil.Process:
        """

        :return:
        """
        process = psutil.Process(self.pid)
        # Parent is pycharm.sh for PyCharm
        parent = process.parent()

        # pycharm.sh has a single child (main Java process)
        java_process = parent.children()[0]

        # Find the "newest" child of the Java process, as that is likely the one we want
        newest = None
        newest_time = -float("inf")
        for child in self._process_filter(java_process.children()):
            if newest is None or newest_time < child.create_time() and len(child.cmdline()) > 1:
                newest = child

        # If no Python process is found we use the calling process
        if newest is None:
            return process

        return newest

    def _get_target_name(self, process: psutil.Process) -> str:
        """

        :return:
        """
        cmdline = process.cmdline()

        if len(cmdline) < 2:
            return cmdline[0]

        script_name = cmdline[1]

        # Running in debugger, meaning the script we're running is not the one we're interested in
        if "plugins/python/helpers/pydev/pydevd.py" in script_name:
            # Debugger: python <long-path>/pydevd.py <some-arguments> --file <the-file-we're-interested-in>
            file_flag_found: bool = False
            for part in process.cmdline()[2:]:
                if file_flag_found:
                    script_name = part
                    break

                if part == "--file":
                    file_flag_found = True

            # If unable to find the actual script name, return the debugger file
            # (or we're not running in the debugger and this is actually what the script is called)

        while script_name.startswith("/"):
            script_name = script_name[1:]

        return script_name

    def _file_header(self, process: psutil.Process) -> str:
        """

        :param process:
        :return:
        """
        ABFUSE = "ABFUSE"
        abfuse_magic_header = f"============ {ABFUSE} ============"

        d = {
            "pid": process.pid,
            "cmdline": process.cmdline(),
            "uids": process.uids(),
            "gids": process.gids(),
            "name": process.name(),
            "process_creation_time": process.create_time(),
            "logfile_creation_time": time.time(),
        }

        header = [
            abfuse_magic_header,
            json.dumps(d, sort_keys=False, indent=2),
            abfuse_magic_header.replace(f"{ABFUSE}", "".join(reversed(f"{ABFUSE}"))),
        ]

        return os.linesep.join(header) + os.linesep

    def open(self, path, flags) -> int:
        # TODO: Deny binary mode

        if path != self._source_path(self._fake_file_name):
            raise fuse.FuseOSError(errno.ENOENT)

        process = self._get_source_process()

        logger.debug(f"Source process: {process}")

        # self._target_path creates a filename based on the current time, and will therefore eventually return a
        # filename that does not exist
        while os.path.exists((correct_path := self._target_path(process))):
            pass

        logger.debug(f"Opening {correct_path} instead of {path}")

        dirname = os.path.dirname(correct_path)
        os.makedirs(dirname, exist_ok=True)

        # Ensure `correct_path` exists and is empty
        with open(correct_path, "w") as f:
            f.flush()

        # Update the latest symlink
        latest_file = os.path.join(dirname, "latest")
        if os.path.exists(latest_file):
            os.remove(latest_file)
        os.symlink(os.path.basename(correct_path), latest_file)

        # Write our header first
        with open(correct_path, "w") as f:
            f.write(self._file_header(process))

        # Open in append mode to ensure Jetbrains don't overwrite our header
        fh = os.open(correct_path, os.O_APPEND | flags)

        self._file_handle_map[self._i] = process, fh
        self._i += 1
        logger.debug(f"Returning file handle {self._i} instead of {fh}")
        return self._i - 1

    def flush(self, path, fh):
        fh = self._get_target_file_handle(fh)
        return os.fsync(fh)

    def release(self, path, fh):
        fh = self._get_target_file_handle(fh)
        return os.close(fh)

    def fsync(self, path, fdatasync, fh):
        # NOTE: No file handle conversion as flush does that
        return self.flush(path, fh)

    def truncate(self, path, length, fh=None):
        # TODO: Truncate up to but not including the header?
        return

    def getxattr(self, path, name, position=0):
        # PyCharm needs this function to be defined but doesn't seem to care that it returns anything meaningful
        raise fuse.FuseOSError(errno.ENODATA)

    # Disable unused operations:
    access = None
    listxattr = None
    opendir = None
    releasedir = None

    # NOTE: Define statfs if you want this filesystem to show in the list of filesystems (df f.ex.)
    statfs = None


def __getattribute__debug(self, item):
    """
    Reverse engineering helper.
    Prints all attempted accesses made to functions and variables that does not start with an underscore.
    Helps to understand what needs to be implemented to support a particular program.
    :param item:
    :return:
    """
    if not item.startswith("_"):
        logger.debug(item)
    return fuse.Operations.__getattribute__(self, item)


def main(mountpoint: str, root: str, fake_file_name: str, debug: bool = False):
    log_level = logging.INFO

    if debug:
        log_level = logging.DEBUG
        Abfuse.__getattribute__ = __getattribute__debug
        logging.getLogger("fuse").setLevel(logging.DEBUG)
        logging.getLogger("fuse.log-mixin").setLevel(logging.DEBUG)

    logger.setLevel(log_level)
    logger.info(f"Mounting {root} at {mountpoint}")

    fuse.FUSE(Abfuse(root, fake_file_name), mountpoint, nothreads=True, allow_other=True, nonempty=True, foreground=debug)
